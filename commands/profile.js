

function cmd(message, args) {
    var msg = "```diff\n";
    msg += "!===== Profile Manager =====!\n";
    if(args.length >= 1) {
        switch(args[0]) {
            case "help":
                msg += "/profile list [page]\n";
                msg += "/profile view [RobloxID]\n";
                msg += "/profile set [RobloxID] '[Section]' '[Value]'\n";
                msg += "/profile register [AuthCode]\n";
                msg += "     Use this command to link your roblox account with your discord account";
            break;

            case "list":
                msg += "Coming soon...";
            break;

            case "view":
                msg += "Coming soon...";
            break;

            case "set":
                msg += "Coming soon...";
            break;

            case "register":
                msg += "Coming soon...";
            break

            default:
                msg += "Unhandled sub command! use /profile help";
            break;
        }
    }else{
        msg += "Displayname: N/A\n";
        msg += "Dedication Points: N/A\n";
        msg += "Title Name: N/A\n";
        msg += "Description: N/A\n";
        msg += "Raids Attended: N/A";
    }
    msg += "\n!===== End Profile =====!";
    msg += "```";
    message.reply(msg);
}

module.exports = cmd;