/*
    Put all the test code here
*/
const database = require( "../lib/database.js" );
const Discord = require("discord.js");

//console.log("exists.50010: " + database.userExists(50010))

//Creating test users
/*
console.log("Populating database with test users");
for(var i = 0; i < 10; i++) {
    //database.createUser(5000+i);
    try {
        var userID = 5000+i;
        if(database.userExists(userID)) {
            console.log("Loading new user");
            database.getUserData(userID);
        }else{
            console.log("Creating new user");
            database.createUser(userID);
        }
    } catch (e) {
        console.log("error: " + e);
    }
}
*/

function cmd(msg, args) {
    /*
    msg.reply("test: ");
    msg.reply("args length: " + args.length);
    */

    const embed = new Discord.RichEmbed()
    .setTitle("This is your title, it can hold 256 characters")
    .setAuthor("Author Name", "https://i.imgur.com/lm8s41J.png")
    /*
    * Alternatively, use "#00AE86", [0, 174, 134] or an integer number.
    */
    .setColor(0x00AE86)
    .setDescription("This is the main body of text, it can hold 2048 characters.")
    .setFooter("This is the footer text, it can hold 2048 characters", "http://i.imgur.com/w1vhFSR.png")
    .setImage("http://i.imgur.com/yVpymuV.png")
    .setThumbnail("http://i.imgur.com/p2qNFag.png")
    /*
    * Takes a Date object, defaults to current date.
    */
    .setTimestamp()
    .setURL("https://discord.js.org/#/docs/main/indev/class/RichEmbed")
    .addField("This is a field title, it can hold 256 characters",
    "This is a field value, it can hold 2048 characters.")
    /*
    * Inline fields may not display as inline if the thumbnail and/or image is too big.
    */
    .addField("Inline Field", "They can also be inline.", true)
    /*
    * Blank field, useful to create some space.
    */
    .addBlankField(true)
    .addField("Inline Field 3", "You can have a maximum of 25 fields.", true);
    
    msg.channel.send({embed});
}

module.exports = cmd;