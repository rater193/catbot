/*
    Creator: rater193
    Creation Date: 6/28/2017
    Description:
        This is used to handle the database
*/

function cmd(msg, arguments) {
    var retmsg = "";
    if(arguments.length > 0) {
        switch(arguments[0]) {
            case "help":
                retmsg += "Coming soon!";
            break;

            default:
                retmsg += "Invalid sub command, " + arguments[0]+", use /database help!";
            break;
        }
    }else{
        retmsg += "Not enough arguments!";
    }
    msg.reply(retmsg);
}

module.exports = cmd;