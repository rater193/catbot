/*
    Creator: rater193
    Creation Date: 6/8/2017
    Description:
        This generates the list of help messages, extremely usefull!
*/
const helpText = require( "../lib/commands-helpText.js" );
const commands = require( "../lib/commands.js" );

function onCmd(message) {
    var msg = "```diff\n";
    msg += "Help:\n";

    Object.keys(commands).forEach(function(key) {
        msg += "\n!===== "+key+" =====!\n";
        if(helpText[key]) {
            msg += helpText[key];
        }else{
            msg += "[No help text found, ask a developer to add one! (Add the help text to lib/commands-helpText.js)]";
        }
    });
    msg += "```";
    message.reply(msg);
}


module.exports = onCmd;