
const authlib = require("../lib/authlib.js");

function cmd(msg, args) {
    if(args.length > 0) {
        switch(args[0]) {
            case "list":
                var replymsg = "Auth keys: \n";
                var keys = authlib.GetKeys();
                for(var index in keys) {
                    replymsg += index+": "+keys[index].key+"\n";
                }
                msg.reply(replymsg);
            break;

            case "new":
                msg.reply("Created key: " + authlib.GenerateNewAuthKey());
            break;

            case "check":
                msg.reply("Key: " + args[1]+"\nExists: " + authlib.IsKeyRegistered(args[1]));
            break;

            case "clear":
                msg.reply("Clearing " + authlib.GetKeys().length + " keys!");
                authlib.ClearKeys();
            break;

            default:
                msg.reply("Invalid sub command");
            break;
        }
    }else{
        msg.reply("use /auth list to see a list of auth keys");
    }
}

module.exports = cmd;