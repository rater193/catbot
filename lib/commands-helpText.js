
const helpText = {
    ["database"]: "This is used to manage the database\n     Usage: /database help",
    ["help"]: "The general help command",
    ["ping"]: "Use this to test the connectivity to the bot.\n     Usage: /ping",
    ["profile"]: "This is the base orifuke command.\n     Usage: /profile, use /profile help for other sub commands to use.",
    ["test"]: "This command isnt important, just ignore it, or remove it, it doesnt really matter...\n     Usage: /test"
};

module.exports = helpText;