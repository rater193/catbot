/*
    Creator: rater193
    Creation Date: 6/22/2017
    Description:
        This is the guts ofthe HTTP api, this will be how you
        generate responces for http requests

        API Functions:
            GenerateNewResponce([boolean]success)
                This is for generating new responces
            GenerateResponceError([string]reason)
                This generates a generic error responce
            ConvertResponceToJson([string]responce)
                This converts the responce to json format
*/

const httpapi = {}

httpapi.GenerateNewResponce = function(success) {
    var ret = {
        ["Success"]: success
    }
    return ret;
}

httpapi.GenerateResponceError = function(reason) {
    var newResponce = httpapi.GenerateNewResponce(false);
    newResponce.Reason = reason;
    return httpapi.ConvertResponceToJson(newResponce);
}

httpapi.ConvertResponceToJson = function(responce) {
    return JSON.stringify(responce);
}

module.exports = httpapi;