/*
    Creators: rater193
    Creation Date: 6/22/2017
    Description:
      This is the http server manager. This is what handles all the
      http requests, essentially lets roblox ACTUALLY use the bot.

      HTTP Cmd Functions:
        GetUserData
          Required variables:
            [string]  AuthKey
            [int]     UserID
          Returns:
            [string]  Json data for the user, winch can be specified with "SetUserData"
            [null]    returns null if the user does not exsist inside the database.
          Description:
            This returns a json text for the user's data
            If the user doesnt exsist, it will return a json error saying there is no
            user in the database

        SetUserData
          Required variables:
            [string]  AuthKey
            [string]  UserData
                Be careful with this, this will allow you to set custom user data json.
                essentially, you can generate the json inside roblox, and set their
                data to be returned with "GetUserData"
          Returns:
            [bool]    success
          Description:
            This is used for setting users data with custom json.

        InitCommunicationsLayer
          Required variables:
            [string]  PassPhrase
          Returns:
            [string]  AuthKey
                This auth key will be used for each request
          Description:
            This is used for generating an authentication key for API requests
*/

//Constant variables we dont want to change
const http = require('http');
const httpapi = require("./httpapi.js");
const url = require("url");
const database = require( "./database.js" );
const config = require("../Config.js");
const httpport = config.HttpAPIServerPort; // The port we want to expose the HTTP api to
const authlib = require("./authlib.js");

//Other variables we 
var httpserverstorage = {};

//http command functions
var httpcmdfunctions = {
  ["GetUserData"]: function(urldata) {
    // This is the data we are going to return
    var ret = "";
    if(urldata.has("AuthKey")) {
      if(authlib.IsKeyRegistered(urldata.get("AuthKey"))) {
        //Here we are getting the user data
        if(urldata.has("UserID")) {
          return JSON.stringify(database.getUserData(urldata.get("UserID")));
        }else{
          ret = httpapi.GenerateResponceError("UserID not specified!");
        }
      }else{
        //If the key is not registered, then throw an error!
        ret = httpapi.GenerateResponceError("Invalid or expired auth key!" + urldata.get("AuthKey"));
      }
    }else{
      ret = httpapi.GenerateResponceError("AuthKey not specified!");
    }
    return ret;
  },


  ["SetUserData"]: function(urldata) {
    // This is the data we are going to return
    var ret = "";
    if(urldata.has("AuthKey")) {
      if(authlib.IsKeyRegistered(urldata.get("AuthKey"))) {
        //Here we are getting the user data
        if(urldata.has("UserData")) {
          var data = JSON.parse(urldata.get("UserData"));
          if(data["UserID"]) {
            ret = JSON.stringify(httpapi.GenerateNewResponce(true));
            database.saveUser(data);
          }else{
            ret = httpapi.GenerateResponceError("UsetID not found in json table!");
          }
        }else{
          ret = httpapi.GenerateResponceError("UserData not specified!");
        }
      }else{
        //If the key is not registered, then throw an error!
        ret = httpapi.GenerateResponceError("Invalid or expired auth key!" + urldata.get("AuthKey"));
      }
    }else{
      ret = httpapi.GenerateResponceError("AuthKey not specified!");
    }
    return ret;
  },


  ["InitCommunicationsLayer"]: function(urldata) {
    //Here we are making sure we have a valid pass phrase
    //first we create a ewturn object variable;
    var ret = {};
    //next we check if the user has input a "PassPhrase" into the url variables
    if(urldata.has("PassPhrase")) {
      //If so, then we want to make sure they put the correct pass phrase
      if(urldata.get("PassPhrase")==config.HttpAPIPassPhrase) {
        //If everything is correct, then generate a new auth key
        var newKey = authlib.GenerateNewAuthKey();
        var responce = httpapi.GenerateNewResponce(true);
        responce.AuthKey = newKey;
        ret = JSON.stringify(responce);
      }else{
        //If they put the wront passs phrase, then return a json error saying invalid pass phrase
        ret = httpapi.GenerateResponceError("Invalid pass phrase!");
        console.log("Someone ended up putting an invalid pass phrase! phrase used: " + urldata.get("PassPhrase"));
      }
    }else{
      //If they have not put a pass phrase, return a json error, saying no pass phrase given
      ret = httpapi.GenerateResponceError("No pass phrase given!");
    }
    return (ret);
  }
}
//Regstering events
//http server
http.createServer(function (request, result) {
  //result.write(httpcmdfunctions.GetUserData()); //write a response to the client
  var urlpath = "http://127.0.0.1"+request.url;
  //console.log(url.parse(urlpath));
  //This splits the url into the major chunks
  var parsedURL = url.parse(urlpath);
  //This gets the variables
  var query = parsedURL.query;
  //finally this converts the variables to a javascript object
  var urldata = new url.URLSearchParams(query);
  //logging for debugging
  //console.log(urldata);
  var ret = "";
  //Here we are checking if we have an action to choose from
  if(urldata.has("action")) {
    //We want to proceede if the url has a valid url (essentially looking for url variables, so make sure you "?action=SOMEACTIONTOTAKE" to the url)
    if(httpcmdfunctions[urldata.get("action")]) {
      ret = httpcmdfunctions[urldata.get("action")](urldata);
    }else{
      //If you did supply the action, but the action isnt inside httpcmdfunctions then just return an error saying invalid actions
      var generatedstring = "Invalid Action, use one of the following: ";
      for(var index in httpcmdfunctions) {
        generatedstring += index + ", ";
      }
      ret = httpapi.GenerateResponceError(generatedstring);
    }
  }else{
    ret = httpapi.GenerateResponceError("Action not specified");
  }

  result.write(ret);
  result.end(); //end the response
}).listen(httpport); //the server object listens on port 8080

module.exports = httpserverstorage;