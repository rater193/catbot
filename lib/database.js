/*
    Creator: rate193
    Creation DatE: 6/6/2017
    Description:
        This is the main controller for the database system

        Functions:
            getUserData([int]userID)
            userExists([int]userID)
            createUser([int]userID)
            newUserObject([int]userID)
            saveUser([int]userID)

*/

console.log("database load");

const fs = require("fs");
const request = require('sync-request');


var database = {
    ["getUserData"]: function(userID) {
        var filepath = "./database/users/"+userID+".json";
        try {
            if(fs.existsSync(filepath)) {
                var data = fs.readFileSync(filepath);
                return JSON.parse(data);
            }else{
                var newObject = {};
                for(var index in require("../defaultProfile.js")) {
                    newObject[index] = require("../defaultProfile.js")[index];
                }
                return newObject;
            }
        }
        catch (e) {

        }
        return null;
    },
    ["userExists"]: function(userID) { return (fs.existsSync("./database/users/"+userID+".json")) ? true : false; },
    ["createUser"]: function(userID) {
        var userdata = database.newUserObject(userID);
        //console.log("Creating userdata for id, "+userID);
        database.saveUser(userdata);
    },
    ["newUserObject"]: function(userID) {
        //Here we are creating the new user object
        var newUserObject = require("../defaultProfile.js");
        newUserObject.UserID = userID;

        return newUserObject;
    },
    ["saveUser"]: function(userData) {
        if(userData) {
            var data = JSON.stringify(userData);
            fs.writeFile("./database/users/"+userData.UserID+".json", data, function(err) {});
            //console.log("data: "+data);
        }
    }
};

module.exports = database;