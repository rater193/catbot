/*
    Creators: rater193
    Creation Date: 6/10/2017
    Description:
        This is the automated attempt at creating commands
        all you have to do is create a new file in the
        commands directory
*/

const testFolder = './commands/';
const fs = require('fs');

const commands = {};

fs.readdir(testFolder, (err, files) => {
    files.forEach(file => {
        
        if(file.substr(file.length-3)==".js") {
            var filename = file.slice(0, file.length-3);
            commands[filename] = require("../commands/"+file);
            console.log("Registering command: "+filename);
        }
    });
});

module.exports = commands;