/*
    Creator: rater193
    Creation Date: 6/28/2017
    Description:
        This is used for authenticating requests

        API Functions:
            authlib.GenerateNewAuthKey()
                This generates a new key, that will be added to "authKeys" 

            authlib.GenerateNewAuthKeyQuietly()
                This generates a new auth key without handling it.
                Use this for other things that want a new key without
                adding it to the mainstream authKeys.

            authlib.ClearKeys()
                This clears the keys, you can use the auth command to clear the keys
            
            authlib.GetKeys()
                Use this to get a list of all the active keys.
                (Dont change any values in this)

            authlib.IsKeyRegistered([string]authKey)
                Returns true/false depending if the key is, or is not registered.
                Remember to init first to get a new valid key.
*/

//This is the auth chars we want to use
const authChars = require("./authchars.js");
const config = require("../config.js");
//This is the size of the auth key
const authcharlength = config.AuthKeyCharCount;

//This is the list of generated authentication keys
//Use this to see all the current keys in use
var authKeys = [];

//This is our library to return
var authlib = {};

//This stores newly generated keys
//The keys will have a timestamp on them
//(for now) all keys get parsed 

//This generates a new key and adds it to the authKeys list (use this for handling new keys you want to use)
authlib.GenerateNewAuthKey = function() {
    var ret = authlib.GenerateNewAuthKeyQuietly();
    authKeys[authKeys.length] = {
        ["key"]: ret,
        ["timestampToDelete"]: (new Date() / 1000) + (config.AuthKeyMaxTime)
    };
    return ret;
}

//Here we are generating a new key without adding it to the authKeys storage
authlib.GenerateNewAuthKeyQuietly = function() {
    var ret = "";
    for(var i = 0; i < authcharlength; i++) {
        ret += authChars[Math.floor((Math.random() * (authChars.length-0.001)))];
    }
    return ret;
}

authlib.ClearKeys = function() {
    authKeys = [];
}

authlib.GetKeys = function() {
    return authKeys;
}

authlib.IsKeyRegistered = function(authKey) {
    for(var index in authKeys) {
        if(authKeys[index].key==authKey) {
            return true;
        }
    }
    return false;
}

module.exports = authlib;

//Here we are starting the timer
setInterval(function() {
    //This is the list of indexes to remove from the array
    var indexesToRemove = [];
    //Here we are deleting the objects that have expired
    for(var index in authKeys) {
        //Here we are checking if our current timestamp is greater than the timestamp to delete the auth key
        if((new Date() / 1000) >= authKeys[index].timestampToDelete) {
            delete authKeys[index];
            indexesToRemove[indexesToRemove.length] = index;
        }
    }
    console.log("removing " + indexesToRemove.length + " auth keys!");
    //Here we are removing any null values from the list
    for(var index = indexesToRemove.length-1; index >= 0; index--) {
        authKeys.splice(indexesToRemove[index], 1);
    }
}, config.AuthKeyCleanerInterval);