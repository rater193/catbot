/*
    Creators: rater193, Shawnyg
    Creation Date: 6/8/2017
    Description:
        This is the cat bot
*/

//Put required libraries here
//node modules
const discord = require('discord.js');
//const path = require('path');

//libraries
const database = require( "./lib/database.js" );
const commands = require( "./lib/commands.js" );
const httpservice = require( "./lib/httpserver.js" );

//config
const config = require("./Config.js");

//Other main instances that are going to be used constantly
const client = new discord.Client();
//command functions

//Regstering events
//Discord bot
client.on('ready', () => {
    console.log('I am ready!');
});

//This a VERRY basic command example
//i will expand on this in the future
client.on('message', message => {
    //This is the first character of the message, we want to check if the user "/"
    var firstCharacter = message.content[0];
    if(firstCharacter=="/") {
        //Here we are getting rid of the first character
        var args = message.content.split(" ");
        var msg = args [0].substr(1);
        //Removing the first item in the array
        args = args.splice(1,args.length-1);
        //Here we are checking if we have the command avalible
        if(commands[msg]) {
            var targetFunction = commands[msg];
            targetFunction(message, args);
        }else{
            message.reply("Unhandled command, " + msg);
        }
        console.log("msg : "+msg);
        
    }
});

//Finally finishing the bot, by loging in
client.login(config.DiscordBotHttpAPI);
