
module.exports = {
    //HTTP Api
    ["HttpAPIServerPort"]: 21992,
    ["HttpAPIPassPhrase"]: "ChangeBeforeUsingThis!", // This is the pass phrase for generating new keys.

    //Security
    ["AuthKeyCharCount"]: 16, //The ammount of random characters for the auth keys
    ["AuthKeyMaxTime"]: 60*60*4, // This makes the keys stay for 1 hr
    ["AuthKeyCleanerInterval"]: 1000*60, // This makes the auth key cleaner check once per 60 seconds

    //Discord bot
    ["DiscordBotHttpAPI"]: "NDEyMDU3ODM2NDI0NjU4OTU0.XTCFwA.LBmnkMW5uA-NC7B7POo9qzGhCQ8"
};